;
; VICERA Snake by h34ting4ppliance
;
; snake.s
;
; This file contains all the functions for the Snake game itself.
;

; Definitions
; Apple location
.define APPLE,		0xa000
; Snake size
.define SNAKESZ,	0xa002
; Snake parts X
.define SNAKEPX,	0xb000
; Snake parts Y
.define SNAKEPY,	0xb100
; Direction register
.define SDIR,		0xa003

; Directions
.define SLEFT,		0x10
.define SDOWN,		0x20
.define SRIGHT,		0x40
.define SUP,		0x80

; Moves the apple
; B -> X
; C -> Y
snake.mv_apple:
    push hl
    push bc
    mov b, SCRSIZE

	; Gets random X and Y position
    call spritemgr.clrscr
    call math.random
    call math.mod       ; Random integer between 0 and 20
    mov h, a
    call math.random
    call math.mod
    mov c, a
    mov b, h

	; Puts the data into the APPLE variable
    mov hl, APPLE
    mov (hl), b
    inc hl
    mov (hl), c

    pop bc
    pop hl
    ret

; Get head
; Allows to point at the end of the snake array
snake._head:
	push bc

	; sets bc to SNAKESZ variable
	mov b, 0			; Only a byte so b doesn't count
	mov a, (SNAKESZ)
	mov c, a

	; Adding to HL
	add hl, bc
	dec hl

	pop bc
	ret

; hl = hl + C;
; Allows to point at a specific element of the array.
snake._arpoint:
	push bc

	mov b, 0

	add hl, bc
	pop bc
	ret

; Grows the size of the snake
snake.grow_snake:
	push hl

	mov hl, SNAKEPX
	call snake._head
	mov a, (hl)
	inc hl
	mov (hl), a
	mov hl, SNAKEPY
	call snake._head
	mov a, (hl)
	inc hl
	mov (hl), a

	mov a, (SNAKESZ)
	inc a
	mov (SNAKESZ), a

	pop hl
	ret

; Check if the snake head is on the APPLE
; A -> Ate it?
; (1 = True; 0 = False)
snake.ate_apple:
	pusha

	; Gets the location of the snake head
	mov hl, SNAKEPX
	call snake._head
	mov b, (hl)
	mov hl, SNAKEPY
	call snake._head
	mov c, (hl)

	; Gets the location of the apple
	mov hl, APPLE
	mov d, (hl)
	inc hl
	mov e, (hl)

	; Are they equal?
	mov a, d
	cp b
	; If not jump to set the variable false
	jn snake.ate_apple_false

	mov a, e
	cp c
	jn snake.ate_apple_false

	mov a, 1
	jp snake.ate_apple_end
	
	snake.ate_apple_false:
		mov a, 0
	snake.ate_apple_end:
		popa
		ret

; Moves the snake to the good direction
snake.mv_snake:
	pusha
	mov a, (SNAKESZ)
	cp 1
	jz snake.mv_snake_head

	mov c, 0
	snake.mv_snake_loop:
		inc c
		
		; for (i = 1; i < head - snake; ++i) snake[i-1] = snake[i];
		mov hl, SNAKEPX
		mov l, c
		mov a, (hl)
		dec hl
		mov (hl), a
		
		mov hl, SNAKEPY
		mov l, c
		mov a, (hl)
		dec hl
		mov (hl), a
		
		mov a, (SNAKESZ)
		dec a
		mov b, a
		mov a, c
		cp b
		jn snake.mv_snake_loop

		; TODO: Finish this procedure.

	snake.mv_snake_head:
		mov hl, SNAKEPX
		call snake._head
		swap de
		mov hl, SNAKEPY
		call snake._head
		
		mov a, (SDIR)
		cp SLEFT	; Left
		jz snake.mv_snake_head._left
		cp SRIGHT	; Right
		jz snake.mv_snake_head._right
		cp SDOWN	; Down
		jz snake.mv_snake_head._down
		cp SUP		; Up
		jz snake.mv_snake_head._up
		
	snake.mv_snake_end:
		popa
		ret

	; Go left
	snake.mv_snake_head._left:
		swap de
		mov a, (hl)
		dec a
		mov (hl), a
		
		jp snake.mv_snake_end
	; Go right
	snake.mv_snake_head._right:
		swap de
		mov a, (hl)
		inc a
		mov (hl), a
		
		jp snake.mv_snake_end
	; Go up
	snake.mv_snake_head._up:
		mov a, (hl)
		dec a
		mov (hl), a

		jp snake.mv_snake_end
	; Go down
	snake.mv_snake_head._down:
		mov a, (hl)
		inc a
		mov (hl), a

		jp snake.mv_snake_end

; Draws the screen
snake.draw_screen:
	pusha
	
	; Clear the screen
	call spritemgr.clrscr

	; Draw the apple
	mov hl, APPLE
	mov b, (hl)
	inc hl
	mov c, (hl)
	mov d, 2
	call spritemgr.xy
	
	; Draw the snake
	mov a, (SNAKESZ)
	mov c, a
	mov hl, SNAKEPX
	mov de, SNAKEPY

	snake.draw_screen_loop:
		pusha
		mov b, (hl)
		swap de
		mov c, (hl)
		mov d, 1
		
		call spritemgr.xy
		popa

		inc hl
		inc de
		dec c

		mov a, c
		cp 0
		jn snake.draw_screen_loop

	popa
	ret

snake.wall_collide_x:
	push hl
	mov hl, SNAKEPX
	call snake.wall_collide
	pop hl
	ret

snake.wall_collide_y:
	push hl
	mov hl, SNAKEPY
	call snake.wall_collide
	pop hl
	ret

; Is the snake eating itself?
snake.eating_self:
	pusha
	mov a, (SNAKESZ)
	cp 1
	mov a, 0
	jz snake.eating_self_false

	mov c, 0
	snake.eating_self_loop:
		mov hl, SNAKEPX
		call snake._head
		mov de, SNAKEPX
		mov e, c
		
		mov a, (de)
		cp (hl)
		jn snake.eating_self_loop_f

		mov hl, SNAKEPY
		call snake._head
		mov de, SNAKEPY
		mov e, c

		mov a, (de)
		cp (hl)
		jn snake.eating_self_loop_f

		jp snake.eating_self_true
	snake.eating_self_loop_f:
		inc c
		mov a, (SNAKESZ)
		dec a
		
		cp c
		mov a, 0
		jz snake.eating_self_false
		jp snake.eating_self_loop

	snake.eating_self_true:
		mov a, 1
	snake.eating_self_false:
		popa
		ret

; Is the snake going out the game area?
snake.wall_collide:
	push hl
	
	call snake._head
	mov a, (hl)
	cp 20
	jn snake.wall_collide_2
	
	mov (hl), 0
	jp snake.wall_collide_end

	snake.wall_collide_2:
		cp 0xff
		jn snake.wall_collide_end
		
		mov (hl), 19

	snake.wall_collide_end:
		pop hl
		ret
