; Sprite Management

; Insert tile in the screen
; D -> ID
; B -> X
; C -> Y
spritemgr.xy:
    pusha
    mov hl, 0

    mov e, b
    mov b, 32
    call math.mul

    mov b, 0
    mov c, e
    add hl, bc
    add hl, TILINDEX

    mov (hl), d
    popa
    ret

; Clears the tiles off the screen
spritemgr.clrscr:
    push HL
    push DE

    mov HL, TILINDEX
    mov DE, 0x3ff
    mov a, 0
    _spritemgr.clrscr_loop:
        mov (hl), 0
        inc HL
        dec DE

        cp d
        jn _spritemgr.clrscr_loop
        cp e
        jn _spritemgr.clrscr_loop

    pop DE
    pop HL
    ret

; Moves data to a part of the RAM
; HL -> RAM Location
; BC -> Beginning
; DE -> End
spritemgr._load:
    pusha

    spritemgr.load_l:
        mov a, (bc)
        mov (hl), a
        inc bc
        inc hl

        mov a, b
        cp d
        jn spritemgr.load_l
        mov a, c
        cp e
        jn spritemgr.load_l

    popa
    ret
