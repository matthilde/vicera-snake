; Ion Random algorithm
; Calculate the random number depending of
; it's previous value.

; Random routine
; In: Nothing
; Out:
;   A -> Random value
math.random:
    mov a, (CLOCKREG)
    ret

; Multiplication
; In:
;   B -> Operand A
;   C -> Operand B
; Out:
;   HL -> Result
math.mul:
    push bc
    
    mov hl, 0
    mov a, b
    _math.mul_loop:
        cp 0
        jz _math.mul_end

        add hl, bc
        dec a
        jp _math.mul_loop

    _math.mul_end:
        pop bc
        ret

; Modulus
; A = A % B
math.mod:
    push bc
    swap c

    _math.mod_loop:
        dec c
        inc a

        swap c
        cp 0
        swap c
        jz _math.mod_end
        
        cp b
        jn _math.mod_loop


        xor a
        jp _math.mod_loop
    
    _math.mod_end:
        dec a
        pop bc
        ret