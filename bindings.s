; RAM bindings for the VICERA system.

.define REFRESH,    0xfff0
.define SCROLLX,    0xfff1
.define SCROLLY,    0xfff2
.define CONTROLLER, 0xfff4
.define CLOCKREG,   0xfff5

.define STACKPTR,   0xff00

.define SPRINDEX,   0x8000
.define SPRMEM,     0x8500

.define TILINDEX,   0x8100
.define TILMEM,     0x8700
.define TILMEMS,    0x8708

.define SCRSIZE,    20