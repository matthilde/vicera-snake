;
; VICERA Snake by h34ting4ppliance
;
; VICERA Snake is an implementation of the popular game that was included in
; lots of portable cellphones from the 90s-2000s.
;
; This game must be assembled using the VICERA Assembler (vicasm).
; Git Repo: https://github.com/vicera/vicasm
;
; main.s
;
; Main file containing the main procedure and main loop.
;
mov sp, STACKPTR
jp premain

.db 0x6a, 0x70, 0x70

.include "bindings.s"
.include "math.s"
.include "spritemgr.s"
.include "snake.s"

Tiles:
    .bin "sprites/snake.spr"
TilesEnd:

mainloop:
	pusha

	mov a, (CONTROLLER)
	cp 0
	jz mainloop_preend
	mov (SDIR), a

	mainloop_preend:
		mov a, (REFRESH)
		mov b, 0x08
		call math.mod
		cp 0
		jn mainloop_end

		call snake.mv_snake
		call snake.wall_collide_x
		call snake.wall_collide_y
		
		call snake.eating_self
		cp 1
		jz game_over

		call snake.ate_apple
		cp 1
		jn mainloop_preend_2
		call snake.mv_apple
		call snake.grow_snake

		mainloop_preend_2:
			call snake.draw_screen

	mainloop_end:
		popa
		ret

; Load tiles
premain:
    pusha
    mov hl, TILMEMS
    mov bc, Tiles
    mov de, TilesEnd

    call spritemgr._load

	mov a, 0x0a
	mov (SNAKEPX), a
	mov (SNAKEPY), a
	mov a, 1
	mov (SNAKESZ), a
	mov a, SRIGHT
	mov (SDIR), a
    popa
    call snake.mv_apple
	call snake.draw_screen
main:
	mov a, (REFRESH)
	mov b, a
	
	main.loop:
		mov a, (REFRESH)
		slp
		cp b
		jz main.loop

		mov b, a
		call mainloop
		jp main.loop

game_over:
	; TODO: Make screen
	halt
