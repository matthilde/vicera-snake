# Yes there is a makefile for this shit
FILES = main.s snake.s math.s bindings.s spritemgr.s
SPRITES = sprites/snake.spr.s
ASM = vicasm
EXENAME = snake.rom

snake.rom: $(FILES) $(SPRITES)
	$(ASM) -o sprites/snake.spr $(SPRITES)
	$(ASM) -o $(EXENAME) $(FILES)
