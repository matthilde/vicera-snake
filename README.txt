NOTE: This project is old. I have found it in my backup hard drive and
      thought it'd be a great idea to publish it again. It used to be hosted on
      my own Gitea instance, I didn't backup the repositories back then.

      Now below, the original README.txt of the project.

+--------------+
| VICERA Snake |
+--------------+

VICERA Snake is a port of the popular snake game for the VICERA fantasy
console written in VIC-8P Assembly. ( https://github.com/vicera )

How to play
~~~~~~~~~~~
You play it with the arrow keys, simply.

Building
~~~~~~~~
To build it, you need vicasm (https://github.com/vicera/vicasm). Once you
have downloaded it and built it, run the make command

 $ make
 [ ... some logs ... ]
 $ # Then run it with the VICERA
 $ vicera -r snake.rom
